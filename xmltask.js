let XmlFormatter = function() {};
/*
  Takes an array of integers or strings, and generate an XML document
  that maintains order based on the array such that
  the array
  [1,2,3,4,5,6]
  return an XML document like
  <1>
    <2>
      <3>
        <4>
          <5>
            <6>
            </6>
          </5>
        </4>
      </3>
    </2>
  </1>
*/
XmlFormatter.prototype.createXmlFromTagArray = function(tagArray) {
  if (!Array.isArray(tagArray)) throw new Error("InvalidParameterException: tagArray must be an array");
  if (tagArray.length == 0) return "";

  // implemtation should recursively build open tags, then
  // once all items in the array are processed, recursively
  // close the tags and convert to a string that can be validated
  // then indented
  let xmlString = '';
  for (var i = 0; i < tagArray.length; i++) {
    xmlString += '<' + tagArray[i] + '>';
  }
  for (var j = tagArray.length - 1; j >= 0; j--) {
    xmlString += '</' + tagArray[j] + '>';
  }

  if (this.validateXml(xmlString)) {
    return this.indentXmlString(xmlString);
  }
};
/*
  Tokenize the xmlDocument and ensure basic XML validity,
  such that each tag has a closing tag, and each tag pair is
  correctly closed in order.

  Better way to do would be to use an XML parsing library
*/
XmlFormatter.prototype.validateXml = function(xmlDocument) {
  if (xmlDocument.length > 0 && xmlDocument[0] !== '<')
    return false;
  let formattedXml = '';
  let pointer = 0;
  let currentTag = '';
  let parseState = 0; // 0 = not in tag, 1 = reading tag
  let tagStack = [];
  let openOrClosingTag = 0; // 0 = opening tag, 1 = closing tag
  while (pointer < xmlDocument.length) {
    let ch = xmlDocument[pointer];
    if (parseState === 1) {
      if (ch === '>') {
        // we have fully read in the tag, push it onto the stack
        if (openOrClosingTag === 1 &&
          tagStack[tagStack.length - 1] === currentTag) {
          tagStack.pop();
        } else if (openOrClosingTag === 1 &&
          tagStack[tagStack.length - 1] !== currentTag) {
          // error in the XML document has been found
          return false;
        } else {
          tagStack.push(currentTag);
        }
        // reset the variables
        currentTag = '';
        parseState = 0;
      } else {
        if (ch != '/') // we want to ignore this character to only get
          // the name of the tag
          currentTag += ch;
      }
    } else if (ch === '<') {
      parseState = 1;
      // peek ahead and set if this tag will be an opening or closing tag
      if ((pointer + 1) < xmlDocument.length && xmlDocument[pointer + 1] === '/')
        openOrClosingTag = 1;
      else
        openOrClosingTag = 0;
    }
    pointer++;
  }
  if (tagStack.length == 0)
    return true;
  return false;
};
/*
  Take a string of unformatted XML such as
  "<a><b><c><d><e><f></f></e></d></c></b></a>"
  and return a correct and indented XML version of the text
*/
XmlFormatter.prototype.indentXmlString = function(xmlString) {
  if (!xmlString) throw new Error("InvalidParameterException: xmlString is undefined");
  if (!this.validateXml(xmlString)) throw new Error("InvalidXMLException: xmlString is not valid XML");
  // process XML through a basic parser that detects open, closing tags
  let pointer = 0;
  let indentedXml = '';
  let indentLevel = 0;
  let openingOrClosingTag = 0; // 0 = opening tag, 1 = closing tag
  while (pointer < xmlString.length) {
    let ch = xmlString[pointer];
    if (ch === '<') {
      // peek ahead and see if we are in an opening or closing tag
      if ((pointer + 1) < xmlString.length && xmlString[pointer + 1] === '/') {
        openingOrClosingTag = 1;
        indentLevel--;
      }
      if (indentLevel === 0) {
        // dont need to indent on the first level, just add the character
        indentedXml += ch;
      } else {
        indentedXml += '\t'.repeat(indentLevel) + ch;
      }
    } else if (ch === '>') {
      indentedXml += ch + '\n';
      if (openingOrClosingTag == 0)
        indentLevel++;
      openingOrClosingTag = 0;

    } else {
      indentedXml += ch;
    }
    pointer++;
  }
  return indentedXml;
};

module.exports = XmlFormatter;
