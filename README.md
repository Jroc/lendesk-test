## Notes
I have chosen to use javascript to answer the tests, as that is the language I have
spent most of my day to day time in recently.  If you would prefer the tests in
ruby, or another language, I would be more than happy to translate them.

For XML parsing and processing, I would obviously normally use a library, however
I believed you wanted me to write code to process it.
The method I used for processing XML is obviously not great, as it relies on only
the name of the tag being contained within the tag.  However, I hope it shows the
coding style you are looking for.

## Setup
`
npm install
`
# Testing
Unit Testing has been done through mocha and chai, which can be executed through:
`
npm test
`
Code coverage can be calculated from istanbul, with:
`
istanbul cover _mocha -R test/*
`
For convience, since this has to be installed as a global dependency, here is the output of istanbul:

=============================== Coverage summary ===============================

Statements   : 81.99% ( 132/161 )

Branches     : 73.91% ( 68/92 )

Functions    : 91.67% ( 11/12 )

Lines        : 87.41% ( 125/143 )

================================================================================

For a production environment, I would want coverage above 90%, but time has been limited so I have included the stats as a place where I would start from.  I would have preferred to implement more bad input tests,
as well as test input limits, however I believe you can see from the unit tests I've written what my approach might be.

## Running Tasks
For convience, you can run each task using the following:
`
node runner.js
`
which will run through each of the tasks as described.

# Task 3 - SQL
`
select d.name, (select count(*) from bones where bones.dog_id = d.id) as num_bones, (select avg(rating) from bones where bones.dog_id = d.id) as avg_rating from dogs as d;
`

Unfortunately, I've not used Ruby enough to be able to confidently tackle the ActiveRecord question in the time constraints, however I would be more than happy to provide code samples where I use ORM tools.

# Additonal Review
If the code samples aren't enough, I would be happy to run through open source examples of work I've done.  The Ginsberg project open sourced the front end of the project, which is an angular codebase and I would be happy to talk through any of the code.
You can view the code at:
https://github.com/ProjectGinsberg/dashboard-open

Additionally, the code for the ionic mobile app we worked on is also open source, which you can view at:
https://github.com/ProjectGinsberg/mobile-cordova-open

While I worked on all areas of the back and front end of the project, I only worked on integrating our angular web tier into the ionic portion of the mobile app, but would be happy to talk through any of the code in either project.