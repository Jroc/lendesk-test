let XmlFormatter = require('./xmltask');
let xmlFormatter = new XmlFormatter();
console.log('from an array like [1, 2, 3, 4, 5, 6, 7], output well formatted valid XML')
console.log(xmlFormatter.createXmlFromTagArray([1, 2, 3, 4, 5, 6, 7]));
console.log('------------------------------------------------')
console.log('from a string like <1><2><3><4><5><6></6></5></4></3></2></1> output well formatted valid xml');
console.log(xmlFormatter.indentXmlString("<1><2><3><4><5><6></6></5></4></3></2></1>"))
console.log('------------------------------------------------')
console.log('read a CSV, outputing a line of text describing each shape, calculating area and perimeter for each')
let GeometryTask = require('./geometrytask');
let y = new GeometryTask();
y.processCsv('geometry.csv');
