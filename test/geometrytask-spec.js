var chai = require('chai');

let GeometryTask = require('../geometrytask');

describe('GeometryTask', function() {
  describe('#calculateApothem()', function() {
    it('should calculate correct apothem for square', function() {
      let geometryTask = new GeometryTask();
      let result = geometryTask.calculateApothem(2, 4);
      chai.expect(result).to.equal(1);
    });
    it('should calculate correct apothem for triangle', function() {
      let geometryTask = new GeometryTask();
      let result = geometryTask.calculateApothem(2, 3);
      chai.expect(result).to.equal(0.58);
    });
    it('should throw error for invalid shape side count', function() {
      let geometryTask = new GeometryTask();

      chai.expect(() => {
        geometryTask.calculateApothem(2, 1)
      }).to.throw("InvalidParameterException: sideCount must be >= 3")
    });
    it('should throw error for invalid shape length', function() {
      let geometryTask = new GeometryTask();

      chai.expect(() => {
        geometryTask.calculateApothem(0, 5)
      }).to.throw("InvalidParameterException: sideLength must be > 0")
    });
    it('should throw error for invalid shape length', function() {
      let geometryTask = new GeometryTask();

      chai.expect(() => {
        geometryTask.calculateApothem(-10, 5)
      }).to.throw("InvalidParameterException: sideLength must be > 0")
    });
  });
  describe('#calculatePolygonArea()', function() {
    it('should correctly calculate polygon area', function() {
      let geometryTask = new GeometryTask();
      chai.expect(geometryTask.calculatePolygonArea(75, 10.32)).to.equal(387)
    });
  });
  describe('#calculatePolygonPerimeter()', function() {
    it('should correctly calculate polygon perimeter', function() {
      let geometryTask = new GeometryTask();
      chai.expect(geometryTask.calculatePolygonPerimeter("triangle", 2)).to.equal(6)
    });
  });
  describe('#calculateCirclePerimeter()', function() {
    it('should correctly calculate circle perimeter', function() {
      let geometryTask = new GeometryTask();
      chai.expect(geometryTask.calculateCirclePerimeter(10)).to.equal(62.83)
    });
  });
  describe('#calculateCircleArea()', function() {
    it('should correctly calculate circle area', function() {
      let geometryTask = new GeometryTask();
      chai.expect(geometryTask.calculateCirclePerimeter(10)).to.equal(62.83)
    });
  });
  describe('#processRecord()', function() {
    it('should correctly process a shape', function() {
      let geometryTask = new GeometryTask();
      let shape = {
        name: "triangle",
        size: 5
      };
      let result = geometryTask.processRecord(shape, 1)
      chai.expect(result.name).to.equal("triangle");
      chai.expect(result.size).to.equal(5);
      chai.expect(result.perimeter).to.equal(15);
      chai.expect(result.area).to.equal(10.8);
    });
    it('should not process a shape with an invalid size', function() {
      let geometryTask = new GeometryTask();
      let shape = {
        name: "triangle",
        size: "red"
      };
      chai.expect(() => {
        geometryTask.processRecord(shape, 1)
      }).to.throw("InvalidParameterException: must be integers")
    });
    it('should not process a shape with an invalid shape', function() {
      let geometryTask = new GeometryTask();
      let shape = {
        name: "red",
        size: 50
      };
      chai.expect(() => {
        geometryTask.processRecord(shape, 1)
      }).to.throw("InvalidParameterException:Shape is not supported")
    });
  });
  describe('#processCsv()', function() {
    it('process a csv file', function() {
      let geometryTask = new GeometryTask();

      let result = geometryTask.processCsv("./geometry.csv");
      console.log(result);
    });
  });
});
