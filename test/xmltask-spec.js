var chai = require('chai');

let XmlFormatter = require('../xmltask');

describe('XmlFormatter', function() {
  describe('#createXmlFromTagArray()', function() {
    it('should do nothing with an empty array', function() {
      let xmlFormatter = new XmlFormatter();
      chai.expect(xmlFormatter.createXmlFromTagArray([])).to.equal("");
    });
  });
  describe('#validateXml()', function() {
    it('should validate proper xml', function() {
      let xmlFormatter = new XmlFormatter();
      chai.expect(xmlFormatter.validateXml('<1></1>')).to.equal(true);
    });
    it('should not validate improper xml', function() {
      let xmlFormatter = new XmlFormatter();
      chai.expect(xmlFormatter.validateXml('<1></2>')).to.equal(false);
    });
    it('should validate repeating but valid xml', function() {
      let xmlFormatter = new XmlFormatter();
      chai.expect(xmlFormatter.validateXml('<1><1></1></1>')).to.equal(true);
    });
    it('should validate longer valid xml', function() {
      let xmlFormatter = new XmlFormatter();
      chai.expect(xmlFormatter.validateXml('<1><2><3><4><5><6></6></5></4></3></2></1>')).to.equal(true);
    });
  });
  describe('#indentXmlString()', function() {
    it('should throw exception when the value is not present', function() {
      let xmlFormatter = new XmlFormatter();
      chai.expect(() => {
        xmlFormatter.indentXmlString()
      }).to.throw("InvalidParameterException: xmlString is undefined")
    });

    it('should throw exception when the value is not valid XML', function() {
      let xmlFormatter = new XmlFormatter();
      chai.expect(() => {
        xmlFormatter.indentXmlString("abc123")
      }).to.throw("InvalidXMLException: xmlString is not valid XML")
    });
    it('should indent valid XML', function() {
      let xmlFormatter = new XmlFormatter();
      chai.expect(xmlFormatter.indentXmlString("<1><2></2></1>"))
        .to.equal("<1>\n\t<2>\n\t</2>\n</1>\n")
    });
    it('should indent more complex valid XML', function() {
      let xmlFormatter = new XmlFormatter();
      chai.expect(xmlFormatter.indentXmlString("<1><2><3><4><5><6></6></5></4></3></2></1>"))
        .to.equal("<1>\n\t<2>\n\t\t<3>\n\t\t\t<4>\n\t\t\t\t<5>\n\t\t\t\t\t<6>\n\t\t\t\t\t</6>\n\t\t\t\t</5>\n\t\t\t</4>\n\t\t</3>\n\t</2>\n</1>\n")
    });
  });
});
