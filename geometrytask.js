/*
  calculate area and perimeter for polygons, and circles
  assumes the input polygon has sides of equal length
*/
let GeometryTask = function() {
  // create a hashtable lookup for supported shape types
  let shapeSides = {};
  shapeSides['triangle'] = 3;
  shapeSides['square'] = 4;
  shapeSides['pentagon'] = 5;
  shapeSides['hexagon'] = 6;
  shapeSides['heptagon'] = 7;
  shapeSides['octagon'] = 8;
  shapeSides['nonagon'] = 9;
  shapeSides['decagon'] = 10;
  // define shapeSides as a readonly property
  let me = this;
  Object.defineProperty(me, "shapeSides", {
    value: shapeSides,
    writable: false
  });
};
/*
  apothem = length of side / (2 * tan(180 / number of sides))
  tan in degrees

  source: http://www.mathopenref.com/apothem.html
*/
GeometryTask.prototype.calculateApothem = function(sideLength, sideCount) {
  if (isNaN(sideLength) || isNaN(sideCount)) throw new Error("InvalidParameterException: must be integers")
  if (sideLength <= 0) throw new Error("InvalidParameterException: sideLength must be > 0")
  if (sideCount < 3) throw new Error("InvalidParameterException: sideCount must be >= 3")

  // javascripts Math.tan takes radians, so we need to do a little converting
  let getTanDeg = (deg) => {
    var rad = deg * Math.PI / 180;
    return Math.tan(rad);
  };
  return Math.round(sideLength / (2 * getTanDeg(180 / sideCount)) * 100) / 100;
};
/*
  area of a polygon = 0.5 * perimeter * apothem
*/
GeometryTask.prototype.calculatePolygonArea = function(perimeter, apothem) {
  if (isNaN(perimeter) || isNaN(apothem)) throw new Error("InvalidParameterException: must be integers")
  if (perimeter <= 0) throw new Error("InvalidParameterException: perimeter must be > 0")
  if (apothem <= 0) throw new Error("InvalidParameterException: apothem must be > 0")

  return Math.round(0.5 * perimeter * apothem * 100) / 100;
};
/*
  perimeter of a polygon = number of sides * length of side
  assumes input polygons sides are always of equal length
*/
GeometryTask.prototype.calculatePolygonPerimeter = function(shapeName, sideLength) {
  if (isNaN(sideLength)) throw new Error("InvalidParameterException: must be integer")
  if (sideLength <= 0) throw new Error("InvalidParameterException: sideLength must be > 0")


  return Math.round(this.shapeSides[shapeName] * sideLength * 100) / 100;
};
/*
  perimeter (or circumference) of a circle = 2 * pi * radius
*/
GeometryTask.prototype.calculateCirclePerimeter = function(radius) {
  if (isNaN(radius)) throw new Error("InvalidParameterException: must be integer")
  if (radius <= 0) throw new Error("InvalidParameterException: radius must be > 0");
  return Math.round(2 * Math.PI * radius * 100) / 100;
};
/*
  area of a circle = pi * r^2
*/
GeometryTask.prototype.calculateCircleArea = function(radius) {
  if (isNaN(radius)) throw new Error("InvalidParameterException: must be integer")
  if (radius <= 0) throw new Error("InvalidParameterException: radius must be > 0");
  return Math.round(Math.PI * radius * radius * 100) / 100;
};
/*
  Processes an input shape so that we learn the area and perimeter of the shape,
  then outputs to the console that information, changing depending on if the shape
  is a circle or polygon.
*/
GeometryTask.prototype.processRecord = function(shape, index) {
  console.log(shape.name)
  if (shape.name !== 'circle' &&
    typeof this.shapeSides[shape.name] === 'undefined') throw new Error("InvalidParameterException:Shape is not supported")
  // return either polygon or circle
  let getType = (d) => {
    switch (d.name) {
      case "circle":
        return "circle";
      default:
        return "polygon";
    }
  };

  let resultString = 'Shape ' + index + ' is a ' + shape.name + ' with ';
  if (getType(shape) == "circle") {
    shape.area = this.calculateCircleArea(shape.size);
    shape.perimeter = this.calculateCirclePerimeter(shape.size);
    resultString += ' a radius of ' + shape.size + ', '
  } else if (getType(shape) == "polygon") {
    var polyApothem = this.calculateApothem(shape.size, this.shapeSides[shape.name]);
    shape.perimeter = this.calculatePolygonPerimeter(shape.name, shape.size);
    shape.area = this.calculatePolygonArea(shape.perimeter, polyApothem);
  }
  resultString += 'having a perimeter of ' + shape.perimeter + ' and an area of ' + shape.area + ' units square.';
  console.log(resultString);

  return shape;
};

/*
  read a CSV and generate area and perimeter information for given input shapes
  will fail midway through the CSV if an invalid shape is input, which not be
  the best option.  Alternatives would be to consume the record and continue
  processing, or to pre validate the input CSV rows, then only process rows if
  the whole CSV is validated.
*/
GeometryTask.prototype.processCsv = function(filename) {
  let fs = require('fs');
  // store an array of valid shape names, for validation
  let validShapes = Object.keys(this.shapeSides);
  validShapes.push('circle');
  fs.readFile(filename, (err, data) => {
    if (err) {
      console.log(err);
    }
    let lines = data.toString().split('\n');
    let i = 1;
    lines.forEach((line) => {
      if (line.length == 0) return; // empty line, just ignore

      lineSplit = line.split(',');
      // validate name and the size dimension
      // name should be a non zero length string,
      // size should be a positive integer
      if (validShapes.indexOf(lineSplit[0]) == -1) {
        // this is a shape we dont have the number of sides for, so we should
        // throw an exception
        throw new Error("InvalidInputException: Input CSV contains unknown shape type")
      }
      if (isNaN(lineSplit[1])) {
        // invalid input on the second option
        throw new Error("InvalidInputException: Input CSV contains non numeric side length")
      }
      var shape = {
        name: lineSplit[0],
        size: +(lineSplit[1])
      };

      this.processRecord(shape, i)
      i++;
    });
  });
};

module.exports = GeometryTask;
